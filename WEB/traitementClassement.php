<!--partie code traitement avec BDD-->
<h6 class="liguey"  > <strong>
<?php
try {

    //connection à la BDD
              include ("connexion.inc.php");
              if ($connecter->connect_errno)
              {
                echo "Echec lors de la connexion à MySQL : (" . $connecter-> connect_errno . ") " . $connecter->connect_error;
              }else
              {   $reg=$connecter->query( "SELECT * FROM poster ORDER BY nbr_vote desc");
                  $rows=mysqli_num_rows($reg);
                  if ($rows!=0)
              ?>

                <center>
                     <div container_fluid>
                      <br>
                      <br>
               	<table border="1" width="40%">
                  <style>
                  table thead tr th { background:#FFFF66;/jaune/ } /* thead : 1ere ligne */
                  table thead tr th:first-child { background:#FFFF66; /gris/ } /* thead : 1ere ligne, 1ere colonne */
                  table tbody tr td { background:#fff; } /* tbody : cellules courantes */
                  </style>
               	<thead>
               		<tr>

               			<th>PING projects</th>
               			<th>Votes</th>
               		</tr>
               	</thead>
               	<tbody>
               <?php
               	// pour chaque ligne (chaque enregistrement)
                while($tuple=$reg->fetch_assoc())
               	{
               		// DONNEES A AFFICHER dans chaque cellule de la ligne
               ?>
               		<tr>

               			<td><?php echo $tuple['nom_proj']; ?></td>
               			<td><?php echo $tuple['nbr_vote']; ?></td>
               		</tr>
               <?php
               	} // fin foreach
               ?>
               	</tbody>
               	</table>
              </div>
              </center>
               <?php

             }
           }catch(Exception $e)
          {
      // En cas d'erreur, on affiche un message et on arrête tout
            die('Erreur : '.$e->getMessage());
          }
?>
</strong></h6>
